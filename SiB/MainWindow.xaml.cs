﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace SiB
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<ToolkPoint> Chardata;
        double l;
        double n_a;
        double V;
        int N;
        double step_L;

        // для хранения начального приближения и текущего значения потенциала
        List<double> fi0;
        List<double> fi;
        // для хранения текущего значения первой производной потенциала
        List<double> hi;
        // для хранения промежуточных значения альфа и бета (их будем использовать при обратном ходе прогонки)
        List<double> a;
        List<double> b;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            l = Convert.ToDouble(TextBoxL.GetLineText(0));
            n_a = Convert.ToDouble(TextBoxN_a.GetLineText(0));
            V = Convert.ToDouble(TextBoxV.GetLineText(0));
            N = Convert.ToInt32(TextBoxN.GetLineText(0));
            step_L = l / (N - 1);

            fi0 = new List<double>(N);
            fi = new List<double>(N);
            hi = new List<double>(N);
            a = new List<double>(N);
            b = new List<double>(N);
            List<double> fi_new = new List<double>(N);
            for (int i=0;i<N;i++)
            {
                a.Add(0);
                fi.Add(0);
                hi.Add(0);
                b.Add(0);
                fi0.Add(0);
                fi_new.Add(0);
            }
            double eps = 0.01;
            double var = 0;

            int count = 0;
            do
            {
                count++;
                var = 0;
                // задаём условия на границе
                a[0] = 0;
                b[0] = 0;       
                // прямой ход
                R_K_Method();
                //вычисляем начальное условие
                hi[N - 1] = (V - b[N - 1]) / a[N - 1];
                fi[N - 1] = V;
                //обратный ход
                R_K_Method_inv();
                // поиск максимума отклонения
                for (int i = 0; i < N; i++)
                {
                    if (var < Math.Abs(fi0[i] - fi[i]))
                        var = Math.Abs(fi0[i] - fi[i]);
                }
                // новое приближение
                fi0 = fi;

            } while (var > eps);//проверка на достижение заданной точности

            Chardata = new List<ToolkPoint>();
            for (int i = 0; i < N; i++)
            {
                Chardata.Add(new ToolkPoint { l = (double)l / N * i, u = fi[i] });
            }
            ChartOne.ItemsSource = Chardata;
        }
        public class ToolkPoint
        {
            public double l { get; set; }
            public double u { get; set; }

        }
        double Func_hi(double hi, int i)
        {
            return n_a * (1 + Math.Exp(-fi0[i]) * (b[i] - 1 - fi0[i] + a[i] * hi));
        }
        double Func_a(double a, int i)
        {
            return 1 - a * a * n_a * Math.Exp(-fi0[i]);
        }


        double Func_b(double a, double b, int i)
        {
            return -a * n_a * (b * Math.Exp(-fi0[i]) - 1 + Math.Exp(-fi0[i]) * (1 + fi0[i]));
        }

        void R_K_Method()
        {
            for (int i = 1; i < N; i++)
            {
                // вычисляем коэффициенты для формул метода Рунге-Кутты
                double k1 = step_L * Func_a(a[i - 1], i);
                double l1 = step_L * Func_b(a[i - 1], b[i - 1], i);

                double k2 = step_L * Func_a(a[i - 1] + k1 / 2, i);
                double l2 = step_L * Func_b(a[i - 1] + k1 / 2, b[i - 1] + l1 / 2, i);

                double k3 = step_L * Func_a(a[i - 1] + k2 / 2, i);
                double l3 = step_L * Func_b(a[i - 1] + k2 / 2, b[i - 1] + l2 / 2, i);

                double k4 = step_L * Func_a(a[i - 1] + k3, i);
                double l4 = step_L * Func_b(a[i - 1] + k3, b[i - 1] + l3, i);

                // использование метода
                a[i] = a[i - 1] + (k1 + 2 * k2 + 2 * k3 + k4) / 6.0;
                b[i] = b[i - 1] + (l1 + 2 * l2 + 2 * l3 + l4) / 6.0;
            }
        }
        void R_K_Method_inv()
        {
            for (int i = N - 2; i >= 0; i--)
            {
                // вычисляем коэффициенты для формул метода Рунге-Кутты
                double k1 = -step_L * Func_hi(hi[i + 1], i);
                double k2 = -step_L * Func_hi(hi[i + 1] + k1 / 2, i);
                double k3 = -step_L * Func_hi(hi[i + 1] + k2 / 2, i);
                double k4 = -step_L * Func_hi(hi[i + 1] + k3, i);

                // использование метода
                hi[i] = hi[i + 1] + (k1 + 2 * k2 + 2 * k3 + k4) / 6.0;

                // вычисление потенциала
                fi[i] = a[i] * hi[i] + b[i];
            }
        }
    }
}
